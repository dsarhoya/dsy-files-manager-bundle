<?php

namespace dsarhoya\DSYFilesManagerBundle\Repository;

/**
 * ManagedFileRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ManagedFileRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByNotType($criteria) 
    {
        $qb = $this->createQueryBuilder('mf');
        $qb->andWhere('mf.fileType != :criteria')
            ->setParameter('criteria', $criteria);
        
        $qb->addOrderBy('mf.createdAt', 'desc');
        return $qb->getQuery()->getResult();
    }
}
