<?php

namespace dsarhoya\DSYFilesManagerBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Description of TwigExtentFromTemplateListener
 * extraido de http://stackoverflow.com/questions/20349194/how-to-send-var-to-view-from-event-listener-in-symfony2
 * @author snake77se at dsarhoya.cl
 */
class TwigExtentFromTemplateListener {
    
    protected $twig;
    protected $extendFromTemplate;
    protected $filesManagerParentBreadcrumbs;

    public function __construct(\Twig_Environment $twig, $extendFromTemplate, $filesManagerParentBreadcrumbs)
    {
        $this->twig = $twig;
        $this->extendFromTemplate = $extendFromTemplate;
        $this->filesManagerParentBreadcrumbs = $filesManagerParentBreadcrumbs;
    }
    
     public function onKernelRequest(GetResponseEvent $event)
    {
        $this->twig->addGlobal('extendTemplate', $this->extendFromTemplate);
        $this->twig->addGlobal('filesManagerParentBreadcrumbs', $this->filesManagerParentBreadcrumbs);
    }
}
