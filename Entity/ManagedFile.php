<?php

namespace dsarhoya\DSYFilesManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use dsarhoya\DSYFilesBundle\Interfaces\IFileEnabledEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

/**
 * ManagedFile
 *
 * @ORM\Table(name="managed_file")
 * @ORM\Entity(repositoryClass="dsarhoya\DSYFilesManagerBundle\Repository\ManagedFileRepository")
 */
class ManagedFile implements IFileEnabledEntity
{
    CONST MANAGED_FILE_TYPE_PDF     = 'pdf';
    CONST MANAGED_FILE_TYPE_WORD    = 'word';
    CONST MANAGED_FILE_TYPE_EXCEL   = 'excel';
    CONST MANAGED_FILE_TYPE_IMAGE   = 'image';
    CONST MANAGED_FILE_TYPE_PLAIN   = 'plain';
    CONST MANAGED_FILE_TYPE_VIDEO   = 'video';
    CONST MANAGED_FILE_TYPE_AUDIO   = 'audio';
    CONST MANAGED_FILE_TYPE_SLIDE   = 'slide';
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * New Name file
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $fileKey;
    
    /**
     * Original Name from file
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $originalName;
    
    /**
     * Type file (pdf, 
     *              excel[xls, xlsx], 
     *              word[doc, docx], 
     *              plain[txt, csv, tsv, ...], 
     *              audio[mp3, ogg], 
     *              images[ png, jpg, bmp, ...], 
     *              video[mp4, mpeg, ...]
     * )
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $fileType;
    
    /**
     *
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $filePath;
    
    /**
     * 
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    private $file;
    
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;
    
    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field
     */
    use SoftDeleteableEntity;
    
    /**
     * Get file
     *
     * @return File
     */
    public function getFile(){
        return $this->file;
    }
    
    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return \dsarhoya\DSYFilesManagerBundle\Entity\ManagedFile
     */
    public function setFile(UploadedFile $file){
        $this->file = $file;
        $this->setFileKey(sprintf('%s.%s', md5(time()), $file->guessExtension()));
        $this->setOriginalName($file->getClientOriginalName());
        return $this;
    }
    
    /**
     * Get fileProperties
     *
     * @return \Array
     */
    public function getFileProperties() {
        return array('ACL'=>'public-read');
    }
    
    /**
     * Get fileFullPathName
     *
     * @return string
     */
    public function getFileFullPathName(){
        return $this->getFilePath().'/'.$this->getFileKey();
    }
    
    /**
     * Get filePath
     * 
     * @return string
     */
    public function getFilePath() {
        return $this->filePath;
    }
    
    /**
     * Set filePath
     * 
     * @param string $path
     * @return $this
     */
    public function setFilePath($path) {
        $this->filePath = $path;
        
        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fileKey
     *
     * @param string $fileKey
     *
     * @return ManagedFile
     */
    public function setFileKey($fileKey)
    {
        $this->fileKey = $fileKey;

        return $this;
    }

    /**
     * Get fileKey
     *
     * @return string
     */
    public function getFileKey()
    {
        return $this->fileKey;
    }

    /**
     * Set originalName
     *
     * @param string $originalName
     *
     * @return ManagedFile
     */
    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName;

        return $this;
    }

    /**
     * Get originalName
     *
     * @return string
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    /**
     * Set fileType
     *
     * @param string $fileType
     *
     * @return ManagedFile
     */
    public function setFileType($fileType)
    {
        $this->fileType = $fileType;

        return $this;
    }

    /**
     * Get fileType
     *
     * @return string
     */
    public function getFileType()
    {
        return $this->fileType;
    }
    
    public static function getFileTypes() 
    {
        return [
            self::MANAGED_FILE_TYPE_PDF,
            self::MANAGED_FILE_TYPE_WORD,
            self::MANAGED_FILE_TYPE_EXCEL,
            self::MANAGED_FILE_TYPE_IMAGE,
            self::MANAGED_FILE_TYPE_PLAIN,
            self::MANAGED_FILE_TYPE_AUDIO,
            self::MANAGED_FILE_TYPE_VIDEO,
            self::MANAGED_FILE_TYPE_SLIDE,
        ];
    }
}
