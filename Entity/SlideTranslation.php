<?php

namespace dsarhoya\DSYFilesManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model\Translatable\Translation;
use dsarhoya\DSYFilesManagerBundle\Entity\ManagedFile;

/**
 * Description of SlideTranslation
 *
 * @ORM\Table()
 * @ORM\Entity
 * @author snake77se at dsarhoya.cl
 */
class SlideTranslation
{
    use Translation;
    
    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $summary;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="ManagedFile")
     * @ORM\JoinColumn(name="managed_file_id", referencedColumnName="id")
     */
    private $managedFile;
    
        /**
     * Set title
     *
     * @param string $title
     *
     * @return Slide
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Set summary
     *
     * @param string $summary
     *
     * @return Slide
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }
    
    /**
     * Set managedFile
     *
     * @param \dsarhoya\DSYFilesManagerBundle\Entity\ManagedFile $managedFile
     *
     * @return Slide
     */
    public function setManagedFile(\dsarhoya\DSYFilesManagerBundle\Entity\ManagedFile $managedFile = null)
    {
        $this->managedFile = $managedFile;

        return $this;
    }

    /**
     * Get managedFile
     *
     * @return \dsarhoya\DSYFilesManagerBundle\Entity\ManagedFile
     */
    public function getManagedFile()
    {
        return $this->managedFile;
    }
    
    /**
     *
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     * @Assert\File(
     *      maxSize = "5M",
     *      maxSizeMessage = "La imagen sobrepasa el tamaño permitido 5 MB."
     * )
     * @Assert\Image(
     *      mimeTypes={"image/gif",
                    "image/jpeg",
                    "image/png"},
     *      mimeTypesMessage = "No es una imagen."
     * )
     */
    private $file;
    
    /**
     *
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    
    public function setFile(\Symfony\Component\HttpFoundation\File\UploadedFile $file)
    {
        $this->file = $file;
        
        return $this;
    }
    
    /**
     * Get file
     *
     * @return File
     */
    public function getFile(){
        return $this->file;
    }
}
