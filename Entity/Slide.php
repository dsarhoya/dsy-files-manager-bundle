<?php

namespace dsarhoya\DSYFilesManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model\Translatable\Translatable;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

/**
 * Description of Slide
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="dsarhoya\DSYFilesManagerBundle\Repository\SlideRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @author snake77se at dsarhoya.cl
 */
class Slide
{
    use Translatable;
    
//    CONST PATH_MAIN = 'manager/slider';
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Slider", inversedBy="slides")
     * @ORM\JoinColumn(name="slider_id", referencedColumnName="id")
     */
    private $slider;
    
    /**
     *
     * @ORM\OneToOne(targetEntity="ManagedFile")
     * @ORM\JoinColumn(name="managed_file_id", referencedColumnName="id")
     * @Assert\Valid()
     */
    private $managedFile;
    
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;
    
    /**
     * Hook SoftDeleteable behavior
     * updates deletedAt field
     */
    use SoftDeleteableEntity;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slider
     *
     * @param \dsarhoya\DSYFilesManagerBundle\Entity\Slider $slider
     *
     * @return Slide
     */
    public function setSlider(\dsarhoya\DSYFilesManagerBundle\Entity\Slider $slider = null)
    {
        $this->slider = $slider;

        return $this;
    }

    /**
     * Get slider
     *
     * @return \dsarhoya\DSYFilesManagerBundle\Entity\Slider
     */
    public function getSlider()
    {
        return $this->slider;
    }

    /**
     * Set managedFile
     *
     * @param \dsarhoya\DSYFilesManagerBundle\Entity\ManagedFile $managedFile
     *
     * @return Slide
     */
    public function setManagedFile(\dsarhoya\DSYFilesManagerBundle\Entity\ManagedFile $managedFile = null)
    {
        $this->managedFile = $managedFile;

        return $this;
    }

    /**
     * Get managedFile
     *
     * @return \dsarhoya\DSYFilesManagerBundle\Entity\ManagedFile
     */
    public function getManagedFile()
    {
        return $this->managedFile;
    }

    /**
     *
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     * @Assert\File(
     *      maxSize = "5M",
     *      maxSizeMessage = "La imagen sobrepasa el tamaño permitido 5 MB."
     * )
     * @Assert\Image(
     *      mimeTypes={"image/gif",
                    "image/jpeg",
                    "image/png"},
     *      mimeTypesMessage = "No es una imagen."
     * )
     */
    private $file;
    
    /**
     *
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    
    public function setFile(\Symfony\Component\HttpFoundation\File\UploadedFile $file){
        $this->file = $file;
        
        return $this;
    }
    
    /**
     * Get file
     *
     * @return File
     */
    public function getFile(){
        return $this->file;
    }
}
