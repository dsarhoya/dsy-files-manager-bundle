<?php

namespace dsarhoya\DSYFilesManagerBundle\Form;

use dsarhoya\DSYFilesManagerBundle\Entity\ManagedFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Description of ManagedFileType
 *
 * @author snake77se
 */
class ManagedFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('file', FileType::class, [
                'label'=>'Archivo',
            ])
            ->add('submit', SubmitType::class, array(
                'label' => 'Guardar',
                'attr'  => array(
                    'class' => 'btn btn-primary'
                )
            ))
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => ManagedFile::class,
        ));
    }
    
    public function getBlockPrefix() {
        return 'dsarhoya_files_manager_managed_file';
    }
}
