<?php

namespace dsarhoya\DSYFilesManagerBundle\Form;

use Doctrine\ORM\EntityRepository;
use dsarhoya\DSYFilesManagerBundle\Entity\Slider;
use dsarhoya\DSYFilesManagerBundle\Entity\Slide;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

/**
 * Description of SlideType
 *
 * @author snake77se at dsarhoya.cl
 */
class SliderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', null, [
                'label'=>'Name',
            ])
            ->add('date', DateType::class, [
                'label'=>'Fecha',
            ])
//            ->add('slides', EntityType::class, [
//                'label'=>'Slides',
//                'class'=> Slide::class,
////                'query_builder' => function (EntityRepository $er) use ($options){
////                        /* @var $qb \Doctrine\ORM\QueryBuilder */
////                        $qb =  $er->createQueryBuilder('s');
////                        $qb->select('s, slt')
////                            ->leftJoin('s.translations', 'slt');
//////                        dump($qb);die;
//////                        if(!is_null($options['locale'])){
//////                            $qb->andWhere($qb->expr()->eq('slt.locale', ':locale'))
//////                                ->setParameter('locale', $options['locale']);
//////                        }
////                        dump($qb->getQuery()->getResult());die;
////                    return $qb;
////                },
////                'choice_label' => 'title',
//            ])
//            ->add('submit', SubmitType::class, array(
//                'label' => 'Guardar',
//                'attr'  => array(
//                    'class' => 'btn btn-primary'
//                )
//            ))
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Slider::class,
            'locale'=>null,
        ));
    }
    
    public function getBlockPrefix() {
        return 'dsarhoya_files_manager_slider';
    }
}
