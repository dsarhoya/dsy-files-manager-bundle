<?php

namespace dsarhoya\DSYFilesManagerBundle\Form;

use dsarhoya\DSYFilesManagerBundle\Entity\Slide;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use A2lix\TranslationFormBundle\Form\Type\TranslationsType;

/**
 * Description of SlideType
 *
 * @author snake77se at dsarhoya.cl
 */
class SlideType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
//            ->add('file', FileType::class, [
//                'label'=>'Image ',
//                'required'=>false,
//                'is_image'=> true,
//                'file_url_options'=>array('signed'=>false),
//            ])
            ->add('translations', TranslationsType::class, [
                'label'=>false,
                'fields' => [
                    'file' => [
                        'label'=>'File Translatable'
                    ],
                    'title' => [],
                    'summary' => [],
                ],
            ])
//            ->add('submit', SubmitType::class, array(
//                'label' => 'Guardar',
//                'attr'  => array(
//                    'class' => 'btn btn-primary'
//                )
//            ))
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Slide::class,
            'locale'=>null,
        ));
    }
    
    public function getBlockPrefix() {
        return 'dsarhoya_files_manager_slide';
    }
}
