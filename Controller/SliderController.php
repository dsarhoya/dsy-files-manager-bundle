<?php

namespace dsarhoya\DSYFilesManagerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use dsarhoya\DSYFilesManagerBundle\Entity\Slider;
use dsarhoya\DSYFilesManagerBundle\Entity\Slide;
use dsarhoya\DSYFilesManagerBundle\Entity\ManagedFile;
use dsarhoya\DSYFilesManagerBundle\Form\SliderType;
use dsarhoya\DSYFilesManagerBundle\Form\SlideType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use dsarhoya\DSYFilesManagerBundle\Controller\DSYFMBBaseController;

/**
 * Description of SliderController
 * @Route("/sliders")
 * @author snake77se at dsarhoya.cl
 */
class SliderController extends DSYFMBBaseController
{
    /**
     * Lists all slider entities.
     *
     * @Route("/", name="slider_index")
     * @Method("GET")
     */
    public function getIndexAction(Request $request) {
        $sliders = $this->getRepoSliders()->findBy([]);
        
        return $this->render('dsarhoyaDSYFilesManagerBundle:Slider:index.html.twig', [
            'sliders'=>$sliders,
        ]);
    }
    
    /**
     * Creates a new slider entity.
     * 
     * @Route("/new", name="slider_new")
     * @Method({"POST","GET"})
     */
    public function newAction(Request $request) 
    {
        $slider = new Slider();
        $form = $this->createForm(SliderType::class, $slider, ['locale'=>$request->getLocale()]);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($slider);
            $em->flush();
            $this->flash('Slider successfully created', self::FLASH_TYPE_SUCCESS);
            return $this->redirectToRoute('slider_edit', ['id'=>$slider->getId()]);
        }
        
        return $this->render('dsarhoyaDSYFilesManagerBundle:Slider:new.html.twig', array(
            'slider' => $slider,
            'form' => $form->createView(),
        ));
        
    }
    
    /**
     * Finds and displays a slider entity.
     *
     * @Route("/{id}/show", name="slider_show")
     * @Method("GET")
     */
    public function showAction(ManagedFile $slider)
    {
        return $this->render('dsarhoyaDSYFilesManagerBundle:Slider:show.html.twig', array(
            'slider' => $slider,
        ));
    }
    
    /**
     * Displays a form to edit an existing slider entity.
     *
     * @Route("/{id}/edit", name="slider_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Slider $slider)
    {
        $editForm = $this->createForm(SliderType::class, $slider);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->flash('Slider successfully updated', self::FLASH_TYPE_SUCCESS);
            return $this->redirectToRoute('slider_index', array('id' => $slider->getId()));
        }

        return $this->render('dsarhoyaDSYFilesManagerBundle:Slider:edit.html.twig', array(
            'slider' => $slider,
            'edit_form' => $editForm->createView(),
        ));
    }
    
    /**
     * Delete slider entity
     * @Route("/{id}/delete", name="slider_delete")
     * @Method("GET")
     */
    public function removeAction(Slider $slider) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($slider);
        $em->flush($slider);
        $this->flash('Slider deleted successfully', self::FLASH_TYPE_SUCCESS);
        return $this->redirectToRoute('slider_index');
    }
    
    /**
     * 
     * @Route("/{id}/slides/new", name="slider_slide_new")
     * @Method({"GET","POST"})
     */
    public function newSlideAction(Request $request, Slider $slider) 
    {
        $slide = new Slide();
        $form = $this->createForm(SlideType::class, $slide);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $translations = $form->get('translations')->getData();
            foreach($translations as $locale=>$translation){
                if(null !== $file = $translation->getFile()){
                    $managedFile = new ManagedFile();
                    $managedFile->setFileType(ManagedFile::MANAGED_FILE_TYPE_SLIDE);
                    $managedFile->setFile($file);
                    $em->persist($managedFile);
                    $slide->translate($locale)->setManagedFile($managedFile);
                }
            }
            $slider->addSlide($slide);
            $slide->setSlider($slider);
            $slide->mergeNewTranslations();
            $em->persist($slide);
            $em->flush();
            $this->flash('Slide successfully created', self::FLASH_TYPE_SUCCESS);
            return $this->redirectToRoute('slider_edit', ['id'=>$slider->getId()]);
        }
        return $this->render('dsarhoyaDSYFilesManagerBundle:Slider:sliderSlideNew.html.twig', array(
            'slider' => $slider,
            'form' => $form->createView(),
        ));
    }
    
    /**
     * 
     * @Route("/{id}/slides/{slide}/edit", name="slider_slide_edit")
     * @Method({"GET","POST"})
     */
    public function editSlideAction(Request $request, Slider $slider, Slide $slide) {
        $edit_form = $this->createForm(SlideType::class, $slide);
        $edit_form->handleRequest($request);
        
        if ($edit_form->isSubmitted() && $edit_form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $translations = $edit_form->get('translations')->getData();
            foreach($translations as $locale=>$translation){
                if(null !== $file = $translation->getFile()){
                    $managedFile = new ManagedFile();
                    $managedFile->setFileType(ManagedFile::MANAGED_FILE_TYPE_SLIDE);
                    $managedFile->setFile($file);
                    $em->persist($managedFile);
                    $slide->translate($locale)->setManagedFile($managedFile);
                }
            }
            $slide->mergeNewTranslations();
            $em->persist($slide);
            $em->flush();
            $this->flash('Slide successfully updated', self::FLASH_TYPE_SUCCESS);
            return $this->redirectToRoute('slider_edit', ['id'=>$slider->getId()]);
        }
        return $this->render('dsarhoyaDSYFilesManagerBundle:Slider:sliderSlideEdit.html.twig', array(
            'slider' => $slider,
            'slide' => $slide,
            'edit_form' => $edit_form->createView(),
            'file_path'=>$this->container->getParameter("dsarhoya_dsy_files_manager.files_folder"),
        ));
    }
    
    /**
     * 
     * @Route("/{id}/slides/{slide}/delete", name="slider_slide_delete")
     * @Method({"GET"})
     */
    public function deleteSlideAction(Slider $slider, Slide $slide) {
        $em = $this->getDoctrine()->getManager();
        $slider->removeSlide($slide);
        $em->remove($slide);
        $em->flush($slide);
        $this->flash('Slider deleted successfully', self::FLASH_TYPE_SUCCESS);
        return $this->redirectToRoute('slider_index');
    }
    
    /**
     * 
     * @Route("/{id}/slides/{slide}/delete-managed-file/{locale}", name="slider_slide_managed_file_delete")
     * @Method({"GET"})
     */
    public function deleteSlideManagedFileAction(Slide $slide, $locale) {
        $em = $this->getDoctrine()->getManager();
        $slide->translate($locale)->setManagedFile(null);
        $em->persist($slide);
        $em->flush();
        $this->flash('Image deleted successfully', self::FLASH_TYPE_SUCCESS);
        return $this->redirectToRoute('slider_slide_edit', ['id'=>$slide->getSlider()->getId(), 'slide'=>$slide->getId()]);
    }
}
