<?php

namespace dsarhoya\DSYFilesManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of DSYFMBBaseController
 *
 * @author snake77se at dsarhoya.cl
 */
class DSYFMBBaseController extends Controller
{
    CONST FLASH_TYPE_INFO = 'info';
    CONST FLASH_TYPE_SUCCESS = 'success';
    CONST FLASH_TYPE_ERROR = 'error';
    CONST FLASH_TYPE_WARNING = 'warning';
    
    protected function flash($message, $type = self::FLASH_TYPE_INFO){
        
        $this->get('session')->getFlashBag()->add(
            $type,
            $message
        );
        
    }
    
    protected $class = null;
    protected $bundle = 'dsarhoyaDSYFilesManagerBundle';
    
    protected function repo($class = null, $bundle = null){
        if(!is_string($class)){
            $class = $this->class;
        }
        if(!is_string($bundle)){
            $bundle = $this->bundle;
        }
        return $this->getDoctrine()->getRepository("$bundle:$class");
    }
    
    /**
     * @return \dsarhoya\DSYFilesManagerBundle\Repository\ManagedFileRepository
     */
    public function getRepoManagedFiles(){
        return $this->repo('ManagedFile');
    }
    
    /**
     * @return \dsarhoya\DSYFilesManagerBundle\Repository\SliderRepository
     */
    public function getRepoSliders(){
        return $this->repo('Slider');
    }
    
    /**
     * @return \dsarhoya\DSYFilesManagerBundle\Repository\SlideRepository
     */
    public function getRepoSlides(){
        return $this->repo('Slide');
    }
}
