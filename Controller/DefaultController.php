<?php

namespace dsarhoya\DSYFilesManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('dsarhoyaDSYFilesManagerBundle:Default:index.html.twig', [
            'extendTemplate'=>$this->container->getParameter('dsarhoya_dsy_files_manager.extend_from_template'),
        ]);
    }
}
