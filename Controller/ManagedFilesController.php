<?php

namespace dsarhoya\DSYFilesManagerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use dsarhoya\DSYFilesManagerBundle\Entity\ManagedFile;
use dsarhoya\DSYFilesManagerBundle\Form\ManagedFileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use dsarhoya\DSYFilesManagerBundle\Controller\DSYFMBBaseController;

/**
 * Description of ManagedFilesController
 * @Route("/files-manager")
 * @author snake77se
 */
class ManagedFilesController extends DSYFMBBaseController
{
    /**
     * Lists all managedFiles entities.
     *
     * @Route("/", name="managed_file_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $managedFiles = $em->getRepository('dsarhoyaDSYFilesManagerBundle:ManagedFile')->findByNotType(ManagedFile::MANAGED_FILE_TYPE_SLIDE);
        
        return $this->render('dsarhoyaDSYFilesManagerBundle:ManagedFile:index.html.twig', array(
            'managedFiles' => $managedFiles,
        ));
    }
    
    /**
     * Creates a new managedFiles entity.
     * 
     * @Route("/create/{type}", name="managed_file_create")
     * @Method({"POST","GET"})
     */
    public function createAction(Request $request, $type) 
    {
        if(!in_array($type, $this->container->getParameter('dsarhoya_dsy_files_manager.default_files'))){
            throw new Exception("File type {$type} not implement.");
        }
        
        $managedFile = new ManagedFile();
        $form = $this->createForm(ManagedFileType::class, $managedFile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $file = $form->get('file')->getData();
            if(!is_null($file) && null !== $this->getRepoManagedFiles()->findOneByOriginalName($file->getClientOriginalName())){
                $this->flash('File already exists', self::FLASH_TYPE_INFO);
                return $this->redirectToRoute('managed_file_index');
            }
            $managedFile->setFileType($type);
            $em->persist($managedFile);
            $em->flush($managedFile);
            $this->flash('File successfully uploaded', self::FLASH_TYPE_SUCCESS);
            return $this->redirectToRoute('managed_file_index');
        }

        return $this->render('dsarhoyaDSYFilesManagerBundle:ManagedFile:create.html.twig', array(
            'managedFile' => $managedFile,
            'form' => $form->createView(),
            'type'=>$type,
        ));
        
    }
    
    /**
     * Finds and displays a managedFile entity.
     *
     * @Route("/{id}", name="managed_file_show")
     * @Method("GET")
     */
    public function showAction(ManagedFile $managedFile)
    {
        return $this->render('dsarhoyaDSYFilesManagerBundle:ManagedFile:show.html.twig', array(
            'managedFile' => $managedFile,
        ));
    }
    
    /**
     * Delete managedFile entity
     * @Route("/{id}/remove", name="managed_file_remove")
     * @Method("GET")
     */
    public function removeAction(ManagedFile $managedFile) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($managedFile);
        $em->flush($managedFile);
        $this->flash('File deleted successfully', self::FLASH_TYPE_SUCCESS);
        return $this->redirectToRoute('managed_file_index');
    }
}
