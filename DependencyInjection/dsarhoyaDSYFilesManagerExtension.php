<?php

namespace dsarhoya\DSYFilesManagerBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class dsarhoyaDSYFilesManagerExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        
        $config = $this->processConfiguration($configuration, $configs);
        
        $container->setParameter('dsarhoya_dsy_files_manager.extend_from_template', $config['extend_from_template']);
        $container->setParameter('dsarhoya_dsy_files_manager.files_folder', $config['files_folder']);
        $container->setParameter('dsarhoya_dsy_files_manager.parent_breadcrumbs', $config['parent_breadcrumbs']);
        $container->setParameter('dsarhoya_dsy_files_manager.default_files', $config['default_files']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('doctrine_extensions.yml');
//        $loader->load('validation.yml');
    }
}
