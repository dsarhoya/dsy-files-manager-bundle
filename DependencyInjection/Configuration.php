<?php

namespace dsarhoya\DSYFilesManagerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use dsarhoya\DSYFilesManagerBundle\Entity\ManagedFile;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('dsarhoya_dsy_files_manager');
        $rootNode
            ->children()
                ->scalarNode('extend_from_template')
                    ->info('This value is only used for set the template from which to extend the views of FilesManagerBundle.')
                    ->isRequired()
                ->end()
                ->scalarNode('files_folder')
                    ->info('This value is only used for set the base path to host the files.')
                    ->defaultValue('manager')
                ->end()
                ->scalarNode('parent_breadcrumbs')
                    ->info('This value is only used for set the base parent breadcrumbs Bundle.')
                    ->defaultValue('indexUser')
                ->end()
                ->arrayNode('default_files')
                    ->info('This value is only used for set the types files supported: ["plain","word","excel","pdf","image","audio","video"].')
                    ->defaultValue(ManagedFile::getFileTypes())
                    ->treatNullLike(ManagedFile::getFileTypes())
                    ->prototype('scalar')
                        ->validate()
                        ->ifNotInArray(ManagedFile::getFileTypes())
                            ->thenInvalid('Invalid type file  %s')
                        ->end()
                    ->end()
                ->end()
                
        ;

        return $treeBuilder;
    }
}
