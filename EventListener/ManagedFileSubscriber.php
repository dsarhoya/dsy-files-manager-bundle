<?php

namespace dsarhoya\DSYFilesManagerBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use dsarhoya\DSYFilesManagerBundle\Entity\ManagedFile;

/**
 * Description of ManagedFileSubscriber
 *
 * @author snake77se at dsarhoya.cl
 */
class ManagedFileSubscriber implements EventSubscriber
{
    /**
     *
     * @var string
     */
    private $filespath;
    
    public function __construct($filesPath) {
        $this->filespath = $filesPath;
    }
    
    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'preUpdate',
        );
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }
    
    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // perhaps you only want to act on some "Product" entity
        if ($entity instanceof ManagedFile) {
//            $entityManager = $args->getEntityManager();
            $entity->setFilePath($this->filespath);
        }
    }
}